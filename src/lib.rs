use std::collections::HashMap;
use std::fmt;
use std::fmt::Display;
use r2pipe::{R2Pipe, R2PipeSpawnOptions};
use clap::ArgMatches;

pub fn run(matches: ArgMatches) -> Result<(),String> {

    if let Some(bin) = matches.value_of("path/to/bin") {

        if let Some(times) = matches.value_of("times") {

            let times: u32 = times.trim().parse().unwrap();
            let mut branches = HashMap::new();

           // println!("Total Trials: {}", times);
            for _k in 0..times {
               // println!("Trial Number {}", k + 1);

                let opt = R2PipeSpawnOptions{
                    exepath: "radare2".to_owned(),
                    args: vec!["-2", "-0", "-q", "-d", "-zzz", "-m 0"] //Don't use -AA
                };

                let mut r2p = match R2Pipe::in_session() {
                    Some(_) => R2Pipe::open(),
                    None => R2Pipe::spawn(bin.to_owned(), Some(opt)),
                }.unwrap();
                //Breakpoint at entry ; Continue till reached entry ; seek to entry

                //r2p.cmd("ood");
                r2p.cmd("aaa")?;

                r2p.cmd("db `ie~vaddr[1]`; dc; s `ie~vaddr[1]`")?;
                loop {
                    let aoj = r2p.cmdj("aoj").unwrap();
                    let debugger_info = r2p.cmdj("dij").unwrap();
                    let program_status = debugger_info["type"].as_str().unwrap();
                    //let addresses = r2p.cmdj("iej").unwrap();
                    //let paddr = addresses[0]["paddr"].as_u64().unwrap();

                    if (program_status == "dead") || (program_status == "exit-pid") {
                        break;

                    } else if aoj[0]["type"] == "cjmp" {

                        //cjmp_addrs.push(offset);

                        let address = aoj[0]["addr"].as_u64().unwrap();
                        let fail_address = aoj[0]["fail"].as_u64().unwrap();
                        let jump_address = aoj[0]["jump"].as_u64().unwrap();

                        r2p.cmd("ds ; sr rip")?; //Step into & seek to value of rip


                        let rip_next = r2p.cmdj("drj").unwrap()["rip"]
                            .as_u64().unwrap();

                        if rip_next == jump_address { //Branch Taken
                            if !branches.contains_key(&address){

                                branches.insert(address,
                                                Branch {
                                                    contents: aoj,
                                                    address: address,
                                                    //jump_address,
                                                    //fail_address,
                                                    taken: 1,
                                                    not_taken: 0,
                                                    exception: 0,
                                                    probability: 1.0
                                                });

                            } else {
                                branches.get_mut(&address).unwrap().taken += 1;
                                let taken = branches.get(&address).unwrap().taken;
                                let not_taken = branches.get(&address).unwrap().not_taken;
                                let exception = branches.get(&address).unwrap().exception;

                                //XXX: Overflow Possible
                                branches.get_mut(&address).unwrap().probability = (taken as f32)/ ((taken + not_taken + exception) as f32);

                            }
                        } else if rip_next == fail_address { //Branch Not taken

                            if !branches.contains_key(&address){
                                branches.insert(address,
                                                Branch {
                                                    contents: aoj,
                                                    address: address,
                                                    //jump_address,
                                                    //fail_address,
                                                    taken: 0,
                                                    not_taken: 1,
                                                    exception: 0,
                                                    probability: 0.0
                                                });
                            } else {
                                branches.get_mut(&address).unwrap().not_taken += 1;
                                let taken = branches.get(&address).unwrap().taken;
                                let not_taken = branches.get(&address).unwrap().not_taken;
                                let exception = branches.get(&address).unwrap().exception;

                                //XXX: Overflow Possible
                                branches.get_mut(&address).unwrap().probability = (taken as f32)/ ((taken + not_taken + exception) as f32);


                            }
                        } else { //Exception
                            branches.get_mut(&address).unwrap().exception += 1;
                            let taken = branches.get(&address).unwrap().taken;
                            let not_taken = branches.get(&address).unwrap().not_taken;
                            let exception = branches.get(&address).unwrap().exception;

                            //XXX: Overflow Possible
                            branches.get_mut(&address).unwrap().probability = (taken as f32)/ ((taken + not_taken + exception) as f32);

                        }

                    } else {
                        r2p.cmd("ds ; sr rip")?; //Step into & seek to value of rip
                    }

                }
                r2p.close();
            }

            println!("  {: <15}  {:<20}  {:<8}  {:<12}  {:<12}", "Address", "Instruction","#Taken", "#Not Taken", "P(Taken)");
            for (_, i) in branches.iter() {
                println!("{}", i);
            }

        }
    }

    Ok(())
}
struct Branch {
    contents: serde_json::value::Value,
    address: u64,
    //jump_address: u64,
    //fail_address: u64,
    taken: u32,
    not_taken: u32,
    exception: u32,
    probability: f32
}

impl Display for Branch {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        match self.address {
            _ => write!(f, "0x{address: <15x}  {disasm: <20}  {taken: <8}  {not_taken: <12}  {prob: <12.2}",
                        address = self.address,
                        disasm = self.contents[0]["disasm"].as_str().unwrap(),
                        taken = self.taken,
                        not_taken = self.not_taken,
                        prob = self.probability)
        }
    }

}

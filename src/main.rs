extern crate r2pipe;
extern crate clap;
extern crate serde_json;
extern crate branchtracer;

use std::process;
use clap::{Arg, App};


fn main() {

    let matches = App::new("branchtracer")
        .author("Pranav Vats")
        .version("0.0.1")
        .arg(Arg::with_name("path/to/bin")
             .index(1)
             .required(true))
        .arg(Arg::with_name("times")
             .index(2)
             .default_value("1"))
        .get_matches();

    if let Err(e) = branchtracer::run(matches) {
        eprintln!("E: {}", e);
        process::exit(1);
    }


}
